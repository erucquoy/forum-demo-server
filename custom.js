
/**
 * 
 * @param {import('json-server').JsonServerRouter} app 
 */
 function registerCustomRoutes(app) {
    app.get('/custom-api', (req, res) => {
        console.log('special api')
        res.status(200).end();
    })
}

module.exports = registerCustomRoutes
