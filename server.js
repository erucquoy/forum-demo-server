const jsonServer = require('json-server')
const auth = require('json-server-auth')
const app = jsonServer.create()
const cors = require('cors')

app.use(cors({
  origin: true,
  credentials: true,
  preflightContinue: false,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
}))
app.options('*', cors());

require('./custom')(app)

const rules = auth.rewriter({
    // Permission rules
    users: 600,
    categories: 664,
    forums: 664,
    // Other rules
})
  
// You must apply the middlewares in the following order
app.use(rules)

const router = jsonServer.router('db.json')

// /!\ Bind the router db to the app
app.db = router.db

// You must apply the auth middleware before the router
app.use(auth)

app.use(jsonServer.bodyParser)
app.use((req, res, next) => {
    const now = Date.now()
    if (req.method === 'POST') {
        req.body.createdAt = now;
        req.body.updatedAt = now;
    }
    else if (req.method === 'PUT' || req.method === 'PATCH') {
        req.body.updatedAt = now;
    }
    // Continue to JSON Server router
    next()
})
app.use(router)
app.listen(8122, (err) => {
    if (err) {
        console.error(err)
        process.exit(1)
    }
    console.log('Server listening on http://127.0.0.1:8122')
})
